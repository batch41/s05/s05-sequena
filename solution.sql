-- 1. Return the customerName of the customers who are from the Philippines
SELECT customerName FROM customers WHERE country = "Philippines";

-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3. Return the product name and MSRP of the product named "The Titanic"
SELECT productName, MSRP FROM products where productName = "The Titanic";

-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5. Return the names of customers who have no registered state
SELECT customerName FROM customers WHERE state IS NULL;

-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve.
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8. Return the customer names of customers whose customer names don't have 'a' in them
SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";

-- 9. Return the customer numbers of orders whose comments contain the string 'DHL'
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 10. Return the product lines whose text description mentions the phrase 'state of the art'
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 11. Return the countries of customers without duplication
SELECT DISTINCT country FROM customers;

-- 12. Return the statuses of orders without duplication
SELECT DISTINCT status FROM orders;

-- 13. Return the customer names and countries of customers whose country is USA, France, or Canada
SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

-- 14. Return the first name, last name, and office's city of employees whose offices are in Tokyo
SELECT employees.firstName, employees.lastName, offices.city
    FROM offices JOIN employees ON offices.officeCode = employees.officeCode WHERE city = "Tokyo";

-- 15. Return the customer names of customers who were served by the employee named "Leslie Thopmpson"
SELECT customers.customerName 
    FROM customers JOIN employees ON salesRepEmployeeNumber = employees.employeeNumber WHERE firstName = "Leslie" AND lastName = "Thompson";

-- 16. Return the product names and customer name of products ordered by "Baane Mini Imports"
SELECT products.productName, customers.customerName FROM customers 
    JOIN orders ON customers.customerNumber = orders.customerNumber
    JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
    JOIN products ON orderdetails.productCode = products.productCode WHERE customerName = "Baane Mini Imports";

-- 17. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM employees
    JOIN offices ON employees.officeCode = offices.officeCode
    JOIN customers ON offices.country = customers.country;

-- 18. Return the last names and first names of employees being supervised by "Anthony Bow".
SELECT lastName, firstName FROM employees WHERE reportsTo = 1143;

-- 19. Return the product name and MSRP of the product with the highest MSRP.
SELECT productName, MAX(MSRP) AS MSRP FROM products;

-- 20. Return the number of customers in the UK. 
SELECT COUNT(customerNumber) FROM customers WHERE country = "UK";

-- 21. Return the number of products per product line.
SELECT COUNT(productName), productlines.productLine FROM products 
    JOIN productlines ON products.productLine = productlines.productLine GROUP BY productlines.productLine;

-- 22. Return the number of customers served by every employee.
SELECT COUNT(customerName), employees.employeeNumber FROM customers
    JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber GROUP BY employees.employeeNumber;

-- 23. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.
SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;